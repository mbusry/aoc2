//
//  ViewController.m
//  testapp
//
//  Created by Michael on 10/14/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import "ViewController.h"
#import "AddEventController.h"


@interface ViewController ()

@end

@implementation ViewController



//captures info when we close the addEventView
-(void)DidClose:(NSString *)nameString
{

    textView.text = nameString;
    NSLog(@"DidClose: %@",nameString);

}

- (void)viewDidLoad
{
    
    // load user defaults
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    if (defaults != nil) {
//        
//        NSString *eventField = [defaults objectForKey:@"event"];
//        
//        
//        // reload data
//        textView.text = eventField;
//        NSLog(@"viewDidAppear: %@",eventField);
//        
//    }

    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
        if (defaults != nil) {
    
            NSString *eventField = [defaults objectForKey:@"event"];
    
    
            // reload data
            textView.text = eventField;
            NSLog(@"viewDidAppear: %@",eventField);
            
        }
    mainStorage *event = [mainStorage passEvent];
    eventText = [event eventText];
    NSLog(@"ViewController:\n%@",eventText);
    textView.text = eventText;
    
    rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [swipeLabel addGestureRecognizer:rightSwipe];
    
    
    
    [super viewWillAppear:animated];
}

-(void)onSwipe:(UISwipeGestureRecognizer*) recognizer
{
if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
//        swipeLabel.text = @"swiped right";
        
        AddEventController *viewController = [[AddEventController alloc] initWithNibName:@"addEventView" bundle:nil];
        if (viewController != nil) {
            //
            
//            viewController.delegate = self;
            [self presentModalViewController:viewController animated:TRUE];
        }

        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    if (defaults != nil) {
//        
//        NSString *eventField = [defaults objectForKey:@"event"];
//        
//        
//        // reload data
//        textView.text = eventField;
//        NSLog(@"viewDidAppear: %@",eventField);
//        
//    }

}


// when adding an event this is ran to show the second view called viewController from file addEventView
-(IBAction)onSave:(id)sender
{
// save as userdefaults
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (defaults != nil) {
        NSString *eventField = textView.text;
        
        [defaults setObject:eventField forKey:@"event"];
        
        // saves the data
        [defaults synchronize];
        NSLog(@"ViewController:onSave\n%@",eventField);

    }

    

}

@end
