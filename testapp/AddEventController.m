//
//  AddEventController.m
//  testapp
//
//  Created by Michael on 10/15/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import "AddEventController.h"

@interface AddEventController ()

@end

@implementation AddEventController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [swipeLabel addGestureRecognizer:leftSwipe];
    
    
    
    [super viewWillAppear:animated];
}

-(void)onSwipe:(UISwipeGestureRecognizer*) recognizer
{
    NSLog(@"Entering onSwipe date:%@",datePicker);
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        event = eventField.text;
        NSLog(@"selectedDate length:%@",datePicker);
        
        if (datePicker == NULL)
        {
            NSDate *dateToday = [NSDate date];
            event = eventField.text;
            //        NSString selectedDate = [date description];
            NSLog(@"selectedDate length:%@",datePicker);

            NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];

            [dateFormatter setDateStyle:(NSDateFormatterShortStyle)];
            [dateFormatter setTimeStyle:(NSDateFormatterShortStyle)];
            NSString *selectedDate = [dateFormatter stringFromDate:dateToday];
            NSString *combined = [NSString stringWithFormat:@"%@\n %@",event,selectedDate];
            mainStorage *eventStorage = [mainStorage passEvent];
            [eventStorage setEventText:combined];
//            [self dismissViewControllerAnimated:true completion:nil];

        }else
        {
        //
//            NSDate *dateToday = [NSDate date];
            NSLog(@"Date is set by picker onSwipe date:%@",datePicker);

            event = eventField.text;
            //        NSString selectedDate = [date description];
            NSLog(@"selectedDate length:%@",datePicker);
            
            NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
        
            [dateFormatter setDateStyle:(NSDateFormatterShortStyle)];
            [dateFormatter setTimeStyle:(NSDateFormatterShortStyle)];
            NSString *selectedDate = [dateFormatter stringFromDate:datePicker];
            NSString *combined = [NSString stringWithFormat:@"%@\n%@\n",event,selectedDate];
            NSLog(@"date coming in...onSwipe %@",selectedDate);
            // moved from onChange
            NSLog(@"eventField coming in...onSwipe %@",event);
            mainStorage *eventStorage = [mainStorage passEvent];
            [eventStorage setEventText:combined];
            NSLog(@"combined coming in...onSwipe %@",combined);
        }

        
//*******************************************************************************************
//        Import the mainStorage class, and use
//        mainStorage *event = [mainStorage passEvent];
//        I can then read the variables using
//        eventText = [event eventText];
//        or write to the singleton class using
//        [event setEventText:eventText];
//        and then in another view controller I can do exactly the same again, as using
//        mainStorage *event = [mainStorage passEvent];
//        will return the existing mainStorage.
//*******************************************************************************************
        
        [self dismissViewControllerAnimated:true completion:nil];

    }
}



-(IBAction)onChange:(id)sender;
{
    // Use this to pick a DATE
    
    UIDatePicker *picker = (UIDatePicker*) sender;
    
    if (picker != nil) {
        //
        
        datePicker = picker.date;
        
        NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
        if (dateFormatter != nil)
        {
            [dateFormatter setDateStyle:(NSDateFormatterShortStyle)];
            [dateFormatter setTimeStyle:(NSDateFormatterShortStyle)];
            NSString *todayDate = [dateFormatter stringFromDate:datePicker];
            NSString *combined = [NSString stringWithFormat:@"%@\n %@",eventField.text,todayDate];
            NSLog(@"date picker combined: %@",combined);
            
        }
    }
}

-(IBAction)onClick:(id)sender;
{
    [eventField resignFirstResponder];

}

@end
