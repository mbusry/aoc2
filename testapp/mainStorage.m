//
//  mainStorage.m
//  testApp
//
//  Created by Michael on 10/24/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import "mainStorage.h"

@implementation mainStorage

static mainStorage *passEvent = nil;

@synthesize eventText;

+(mainStorage *)passEvent
{
    if (!passEvent)
    {
        passEvent = [[super allocWithZone:NULL]init];
    }
    return passEvent;
}

+(id)allocWithZone:(NSZone *)zone
{
    return [self passEvent];
}

- (id)init
{
    if (passEvent) 
        return passEvent;
        
        self = [super init];
        
        return self;
}


-(id)retain
{
    return self;
}

-(oneway void) release
{
    // do nothing
}

-(NSUInteger)retainCount
{
    return NSUIntegerMax;
}
@end
