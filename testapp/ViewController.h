//
//  ViewController.h
//  testapp
//
//  Created by Michael on 10/14/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mainStorage.h"

@interface ViewController : UIViewController 

{
    IBOutlet UITextView *textView;
    IBOutlet UILabel *swipeLabel;

    UISwipeGestureRecognizer *rightSwipe;
    NSString *eventText;
    
    
    
}
// added to my Add Event button
-(IBAction)onSave:(id)sender;


@end
