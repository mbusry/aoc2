//
//  mainStorage.h
//  testApp
//
//  Created by Michael on 10/24/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface mainStorage : NSObject
{
    // set any and all variables for the storage here
    NSString *eventText;
}

+(mainStorage*) passEvent;

// set properties for all variables done above
@property (nonatomic, strong) NSString *eventText;


@end
