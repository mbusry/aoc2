//
//  AddEventController.h
//  testapp
//
//  Created by Michael on 10/15/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mainStorage.h"



@interface AddEventController : UIViewController 
{
        
    IBOutlet UILabel *swipeLabel;
    IBOutlet UITextField *eventField;

    
    UISwipeGestureRecognizer *leftSwipe;
    
    NSString *event;

    NSMutableArray *myArray;
    NSDate *datePicker;
}



-(IBAction)onChange:(id)sender;

-(IBAction)onClick:(id)sender;

@end